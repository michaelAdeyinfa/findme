package com.example.misha.wifi;

/**
 * Created by adeymike on 2018-04-22.
 * Class used to create APInfo object, that will store the relevant MAC address and distance
 * calculation for each access point scanned.
 */

public class APInfo {
    private String bssid;
    private float distance;
    private int frequency;
    private int level;

    public APInfo() {

    }

    public APInfo(String bssid, float distance, int frequency, int level) {
        this.bssid = bssid;
        this.distance = distance;
        this.frequency = frequency;
        this.level = level;
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public float getDistance() {
        return (float) distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
