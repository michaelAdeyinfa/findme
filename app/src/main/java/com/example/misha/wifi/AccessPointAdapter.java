package com.example.misha.wifi;

import android.content.Context;
import android.graphics.Color;
import android.net.wifi.ScanResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class AccessPointAdapter extends ArrayAdapter<ScanResult> {

	ImageView accessPointImage;

	public AccessPointAdapter(Context context, List<ScanResult> accesspoints) {
		super(context, 0 ,accesspoints);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

		View listItemView = convertView;

		if (listItemView == null) {
			listItemView = LayoutInflater.from(getContext()).inflate(
					R.layout.accesspoint_list_item, parent, false);
		}

		ScanResult accessPoint = getItem(position);

		accessPointImage = listItemView.findViewById(R.id.signalStrength);

		TextView ssid = listItemView.findViewById(R.id.ssid);
		ssid.setText(accessPoint.SSID);
		TextView bssid = listItemView.findViewById(R.id.bssid);
		bssid.setText(accessPoint.BSSID);
		TextView distance = listItemView.findViewById(R.id.distance);


		float calculatedDistance = calculateDistance(accessPoint.level);
		distance.setText(formatDistance(calculatedDistance));

		if(calculatedDistance < 1.5) {
			accessPointImage.setImageResource(R.drawable.signal_excellent);
			accessPointImage.setColorFilter(Color.GREEN);
		}
		else if(calculatedDistance > 1.5 && calculatedDistance < 2.5) {
			accessPointImage.setImageResource(R.drawable.signal_good);
			accessPointImage.setColorFilter(Color.GREEN);

		}
		else if(calculatedDistance > 2.5 && calculatedDistance < 5) {
			accessPointImage.setImageResource(R.drawable.signal_ok);
			accessPointImage.setColorFilter(Color.parseColor("#ffb733"));

		}
		else if(calculatedDistance > 5 && calculatedDistance < 10) {
			accessPointImage.setImageResource(R.drawable.signal_bad);
			accessPointImage.setColorFilter(Color.parseColor("#ffb733"));

		}
		else {
			accessPointImage.setImageResource(R.drawable.signal_none);
			accessPointImage.setColorFilter(Color.parseColor("#5B0000"));

		}

		return listItemView;

	}

	protected float calculateDistance(int level) {
		int average = -55;
		float num = (float)(average -level)/(float)20;

		double sum = Math.pow(10, num);
		return (float)sum;
	}
	public String formatDistance(float distance) {
		String distanceAsString = String.format("%.1f", distance);
		distanceAsString += "m~";

		return distanceAsString;
	}

}
