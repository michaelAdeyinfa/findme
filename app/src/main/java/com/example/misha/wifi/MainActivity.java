package com.example.misha.wifi;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;

import android.net.ConnectivityManager;
import android.net.wifi.ScanResult;

import android.net.wifi.WifiManager;

import android.os.Build;
import android.os.Bundle;

import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;

import android.util.Log;
import android.view.View;
import android.widget.Button;

import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;

import java.util.List;


/*
    MainActivity Location Tracking, locates the nearest Access Point to your Android Device's location
    (In this case, device used was a OnePlus3T). Displays AP MAC address and estimated distance away.
    LOCATION MUST BE ENABLED IN ORDER FOR DEVICE TO WORK.
     Button may be used for further functionality later on.
* */
public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";

    WifiManager wifiScanner;
    List<ScanResult> listOfScannedAccessPoints = new ArrayList<>();
    List<ScanResult> filteredAccessPointsBySchoolSSID = new ArrayList<>();
    TextView currentLocation;
    DBUtil databaseUtility = new DBUtil();
    AccessPointAdapter mAdapter;


	ArrayList<AccessPoint> returnedListOfAccessPointsFromServer;

    private final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onReceive(Context c, Intent intent) {
            try {
                intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
                if (intent.getAction().equals(wifiScanner.SCAN_RESULTS_AVAILABLE_ACTION)) {
                    listOfScannedAccessPoints = wifiScanner.getScanResults();
                    mAdapter.clear();
                    mAdapter.addAll(listOfScannedAccessPoints);
                    mAdapter.notifyDataSetChanged();
                    for (ScanResult accessPoint : listOfScannedAccessPoints) {
                        filteredAccessPointsBySchoolSSID.add(accessPoint);
                    }
                    String jsonString = (databaseUtility.getJSON("http://adepolo.us-west-2.elasticbeanstalk.com"));
                    convertJsonStringToObject(jsonString);
                    getCurrentRoom();
                    restartScanning();
                }
            } catch (RuntimeException ex) {
                currentLocation.setText(ex.getMessage());
            } finally {
                restartScanning();
            }
        }
    };


    @TargetApi(21)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        permitAllWebsiteCertifications();
        inflateAndSetupGUI();
        instantiateLists();
        startScanning();
    }

    public void inflateAndSetupGUI() {

		ListView accessPointListView = findViewById(R.id.list);

		mAdapter = new AccessPointAdapter(this,listOfScannedAccessPoints);

		accessPointListView.setAdapter(mAdapter);

        currentLocation = findViewById(R.id.lblLocation);

    }


	public void permitAllWebsiteCertifications() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void instantiateLists() {
        returnedListOfAccessPointsFromServer = new ArrayList<>();

    }

    public void clearScannedAndFilteredLists() {
        filteredAccessPointsBySchoolSSID.clear();
        returnedListOfAccessPointsFromServer.clear();
    }

    public void startScanning() {
        try {
            wifiScanner = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            registerReceiver(mWifiScanReceiver,
                    new IntentFilter(wifiScanner.SCAN_RESULTS_AVAILABLE_ACTION));
            wifiScanner.setWifiEnabled(true);
            wifiScanner.startScan();

        } catch (RuntimeException ex) {
            currentLocation.setText(ex.getMessage());
        }
    }

    public void restartScanning() {
        clearScannedAndFilteredLists();
        wifiScanner.startScan();
    }


    public void convertJsonStringToObject(String jsonString) {
        try {
            JSONArray baseJsonArray = new JSONArray(jsonString);

            for (int i = 0; i < baseJsonArray.length(); i++) {

                JSONObject item = baseJsonArray.getJSONObject(i);
                String room = item.getString("ROOM");
                String label1 = item.getString("LABEL_1");
                String label2 = item.getString("LABEL_2");
                String mac1 = item.getString("MAC_1");
                String mac2 = item.getString("MAC_2");
                String mac3 = item.getString("MAC_3");
                String mac4 = item.getString("MAC_4");
                String mac5 = item.getString("MAC_5");

                returnedListOfAccessPointsFromServer.add(new AccessPoint(room, label1, label2, mac1, mac2, mac3, mac4, mac5));
                Log.d(TAG, returnedListOfAccessPointsFromServer.toString());
            }
        } catch (JSONException e) {
            Log.e(TAG, "Problem parsing the JSON results", e);
        }
    }

    public void getCurrentRoom() {
        currentLocation.setText("Keep Searching");

        for (int i = 0; i < 4; i++) {
            for (int x = 0; x < returnedListOfAccessPointsFromServer.size(); x++) {
                if (filteredAccessPointsBySchoolSSID.get(i).BSSID.equals(returnedListOfAccessPointsFromServer.get(x).getMac1())) {
                    currentLocation.setText("Location: " + returnedListOfAccessPointsFromServer.get(x).getRoom());
                }
                if (filteredAccessPointsBySchoolSSID.get(i).BSSID.equals(returnedListOfAccessPointsFromServer.get(x).getMac2())) {
                    currentLocation.setText("Location: " + returnedListOfAccessPointsFromServer.get(x).getRoom());
                }
                if (filteredAccessPointsBySchoolSSID.get(i).BSSID.equals(returnedListOfAccessPointsFromServer.get(x).getMac3())) {
                    currentLocation.setText("Location: " + returnedListOfAccessPointsFromServer.get(x).getRoom());
                }
                if (filteredAccessPointsBySchoolSSID.get(i).BSSID.equals(returnedListOfAccessPointsFromServer.get(x).getMac4())) {
                    currentLocation.setText("Location: " + returnedListOfAccessPointsFromServer.get(x).getRoom());
                }
                if (filteredAccessPointsBySchoolSSID.get(i).BSSID.equals(returnedListOfAccessPointsFromServer.get(x).getMac5())) {
                    currentLocation.setText("Location: " + returnedListOfAccessPointsFromServer.get(x).getRoom());
                }
            }
        }
    }


}
