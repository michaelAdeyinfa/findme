package com.example.misha.wifi;

public class AccessPoint {

    private String room;

    private String label1;
    private String label2;
    private String mac1;
    private String mac2;
    private String mac3;
    private String mac4;
    private String mac5;

    public AccessPoint() {
    }

    public AccessPoint(String room, String label1, String label2, String mac1, String mac2, String mac3, String mac4, String mac5) {
        this.room = room;
        this.label1 = label1;
        this.label2 = label2;
        this.mac1 = mac1;
        this.mac2 = mac2;
        this.mac3 = mac3;
        this.mac4 = mac4;
        this.mac5 = mac5;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getLabel1() {
        return label1;
    }

    public void setLabel1(String label1) {
        this.label1 = label1;
    }

    public String getLabel2() {
        return label2;
    }

    public void setLabel2(String label2) {
        this.label2 = label2;
    }

    public String getMac1() {
        return mac1;
    }

    public void setMac1(String mac1) {
        this.mac1 = mac1;
    }

    public String getMac2() {
        return mac2;
    }

    public void setMac2(String mac2) {
        this.mac2 = mac2;
    }

    public String getMac3() {
        return mac3;
    }

    public void setMac3(String mac3) {
        this.mac3 = mac3;
    }

    public String getMac4() {
        return mac4;
    }

    public void setMac4(String mac4) {
        this.mac4 = mac4;
    }

    public String getMac5() {
        return mac5;
    }

    public void setMac5(String mac5) {
        this.mac5 = mac5;
    }
}


